const puppeteer = require('puppeteer');
const fs = require('fs');

async function getMenuRest2() {
    const browser = await puppeteer.launch({
        headless: true,
        args: ["--no-sandbox", "--disable-setuid-sandbox"]
    });
    const page = await browser.newPage();
    await page.goto(`https://www.crous-reunion.fr/restaurant/ru-hippolyte-foucque/`);
    const menu = await page.evaluate(() => {
        let menu = [];
        let compteur = 0;
        let elements = document.querySelectorAll('div.content.clearfix');
        for (let element of elements) {
            menu.push({
                title: document.querySelectorAll('div#menu-repas h3')[compteur++]?.innerText,
                subtitle1: document.querySelectorAll("div#menu-repas .content > div h4")[1]?.innerText,
                subtitle2: document.querySelectorAll("div#menu-repas .content > div h4")[2]?.innerText,
                plats: element.querySelectorAll('ul.liste-plats')[1]?.innerText,
                dessert: element.querySelectorAll('ul.liste-plats')[2]?.innerText
            });
        }

        return menu;  
    });
    // console.log(menu);
    
    const storeData = (data, path) => {
        try {
            fs.writeFileSync(path, JSON.stringify(data, null, 2));
            //fs.writeFileSync(path, data);
        } catch (err) {
            console.error(err);
        }
    }

    storeData(menu, "./data/rest2.json");

    const readData = (data) => {
        fs.readFile('./data/rest2.json', (err, data) => {
            if (err) throw err;
            data = JSON.stringify(data, null, 2);
            console.log(data);
        });
    }
    
    // readData(menu);

    await browser.close();
}

module.exports = getMenuRest2;
