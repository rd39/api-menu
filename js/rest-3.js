fetch('../data/rest3.json') // './data/data.json'
.then(function (response) {
    return response.json();
})
.then(function (data) {
    appendData(data);
})
.catch(function (err) {
    console.log(err);
});
        
function appendData(data) {
    var mainContainer = document.getElementById("myData");
    for (var i = 0; i < data.length; i++) {
        var div = document.createElement("div");
        div.innerHTML = `
            <h3 class="title"><i class="lni lni-chevron-right-circle"></i>&nbsp;&nbsp;Titre : ${data[i].title}</h3>
            <p class="plats"><i class="lni lni-dinner"></i>&nbsp;&nbsp;Plats : ${data[i].plats}</p>
            <p class="dessert"><i class="lni lni-fresh-juice"></i>&nbsp;&nbsp;Dessert : ${data[i].dessert}</p>
        `;
        mainContainer.appendChild(div);
    }
}