// Imports
const express = require('express');
const server = express();
const cors = require("cors");
const path = require('path');

const data_rest1 = require('./data/rest1.json');
const data_rest2 = require('./data/rest2.json');
const data_rest3 = require('./data/rest3.json');

const getMenuRest1 = require('./routes/rest1');
const getMenuRest2 = require('./routes/rest2');
const getMenuRest3 = require('./routes/rest3');


// Middleware
server.use(express.json());
server.use(cors());

getMenuRest1();
getMenuRest2();
getMenuRest3();

// const timer = setInterval(getMenu, 3600000); // 1 heure 
// const timer = setInterval(getMenu, 60000); // 1 minute 
//console.log('lancement tout les :', timer);

// For parsing application/x-www-form-urlencoded
server.use(express.urlencoded({ extended: true }));

server.use('/css', express.static(__dirname + '/css'));
server.use('/js', express.static(__dirname + '/js'));
server.use('/data', express.static(__dirname + '/data'));
server.use('/pages', express.static(__dirname + '/pages'));
server.use('/index.html', express.static(__dirname + '/index.html'));

// sendFile will go here
server.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
});

// Configure route
server.get('/menu-1', function(req, res) {
    res.setHeader('Content-Type','text/html');
    res.status(200).json(data_rest1);
});

server.get('/menu-2', function(req, res) {
    res.setHeader('Content-Type','text/html');
    res.status(200).json(data_rest2);
});

server.get('/menu-3', function(req, res) {
    res.setHeader('Content-Type','text/html');
    res.status(200).json(data_rest3);
});


// server.get('/menu/:id', (req, res) => {
//     const id = parseInt(req.params.id);
//     const menu = data.find(menu => menu.id === id);
//     res.status(200).json(menu);
// });

// server.post('/menu', (req, res) => {
//     data_rest1.push(req.body);
//     res.status(200).json(data_rest1);
//     res.send(datastring_rest1);

//     data_rest2.push(req.body);
//     res.status(200).json(data_rest2);
//     res.send(datastring_rest2);

//     data_rest3.push(req.body);
//     res.status(200).json(data_rest3);
//     res.send(datastring_rest3);
// });

// server.put('/menu/:id', (req, res) => {
//     const id = parseInt(req.params.id);
//     let menu = data.find(menu => menu.id === id);
//     menu.title = req.body.title,
//     console.log(menu.title);
//     menu.plats = req.body.plats,
//     console.log(menu.plats);
//     menu.dessert = req.body.dessert,
//     console.log(menu.dessert);
//     res.status(200).json(menu);
// });

// server.delete('/menu/:id', (req, res) => {
//     const id = parseInt(req.params.id);
//     let menu = data.find(menu => menu.id === id);
//     data.splice(data.indexOf(menu), 1);
//     res.status(200).json(data);
// });

// Launch server
server.listen(process.env.PORT || 5000, function() {
    console.log('Server started !');
});
